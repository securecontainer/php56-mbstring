# Overview:

mbstring provides multibyte specific string functions that help you deal with multibyte encodings in PHP. In addition to that, mbstring handles character encoding conversion between the possible encoding pairs. mbstring is designed to handle Unicode-based encodings such as UTF-8 and UCS-2 and many single-byte encodings for convenience (listed below)

## Homepage

[PHP-mbstring project](http://php.net/manual/en/book.mbstring.php)

## Notifications (updates):

## Changes done:

## TODO:

